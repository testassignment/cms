import "./App.css";
import Container from "@mui/material/Container";
import Prime from "./components/Prime";
import PrimeList from "./components/PrimeList";
import Box from "@mui/material/Box";
import { useState, useEffect } from "react";
import Axios from "axios";
import Grid from "@mui/material/Grid";

function App() {
  const [valuesResult, setValuesResult] = useState([]);

  const requestPrimeList = () => {
    Axios.post("http://localhost:3001/api/prime/request").then(
      (response) => {
        console.log("response : ", response.data.dataResult);
        setValuesResult(response.data.dataResult);
      },
      (err) => {
        console.log("Error : ", err);
      }
    );
  };
  const onSubmitPrimeRange = () => {
    requestPrimeList();
  };

  useEffect(() => {
    requestPrimeList();
  }, []);

  return (
    <Container maxWidth="false" disableGutters sx={{ p: 4 }}>
      <Box sx={{ mb: 3 }}>
        <Prime onSubmitPrimeRange={onSubmitPrimeRange} />
      </Box>
      <Box
        sx={{
          flexGrow: 1,
          p: 3,
        }}
      >
        <Grid container spacing={3}>
          {valuesResult.map((prop) => {
            return <PrimeList key={prop.ID} {...prop} />;
          })}
        </Grid>
      </Box>
    </Container>
  );
}

export default App;
