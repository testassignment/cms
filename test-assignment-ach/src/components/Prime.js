import * as React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import PropTypes from "prop-types";
import NumberFormat from "react-number-format";
import { useState } from "react";
import Axios from "axios";
import CircularProgress from "@mui/material/CircularProgress";

const NumberFormatCustom = React.forwardRef(function NumberFormatCustom(
  props,
  ref
) {
  const { onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={ref}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator
      isNumericString
    />
  );
});

NumberFormatCustom.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

const Prime = (props) => {
  const [statusSubmit, setStatusSubmit] = useState(false);
  const [values, setValues] = useState({
    startNumber: 0,
    endNumber: 0,
  });

  const handleChange = (prop) => (event) => {
    console.log(prop === "endNumber" ? 1 : 0, " : ", event.target.value);
    if (prop === "startNumber") {
      setValues({
        startNumber: event.target.value,
        endNumber:
          event.target.value > values.endNumber
            ? event.target.value
            : values.endNumber,
      });
    } else {
      setValues({
        ...values,
        [prop]:
          parseInt(event.target.value) > values.startNumber
            ? parseInt(event.target.value)
            : values.startNumber,
      });
    }
  };

  const submitCalculate = () => {
    setStatusSubmit(true);
    console.log(values.startNumber, ":", values.endNumber);
    Axios.post("http://localhost:3001/api/prime/submit", {
      start: parseInt(values.startNumber),
      end: parseInt(values.endNumber),
    }).then(
      (response) => {
        setStatusSubmit(false);
        console.log("response : ", response);
        props.onSubmitPrimeRange();
      },
      (err) => {
        setStatusSubmit(false);
        console.log("Error : ", err);
      }
    );
  };
  return (
    <Box
      sx={{
        flexGrow: 1,
        p: 3,
        backgroundColor: "#dbfaff",
        borderRadius: "10px",
      }}
    >
      <Grid container justifyContent="center" spacing={2}>
        <Grid item xs={5}>
          <TextField
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
            value={values.startNumber}
            sx={{ backgroundColor: "white" }}
            fullWidth
            onChange={handleChange("startNumber")}
            name="numberformat"
            label="Start"
            variant="outlined"
          />
        </Grid>

        <Grid item xs={5}>
          {/* <Grid item xs={12}>
            {new Intl.NumberFormat("en-IN", {
              maximumSignificantDigits: 3,
            }).format(values.endNumber)}
          </Grid> */}
          <TextField
            InputProps={{
              inputComponent: NumberFormatCustom,
            }}
            value={values.endNumber}
            onChange={handleChange("endNumber")}
            sx={{ backgroundColor: "white" }}
            fullWidth
            name="numberformat"
            label="end"
            variant="outlined"
          />
        </Grid>
        <Grid item xs={2}>
          {statusSubmit ? (
            <Button
              sx={{ height: "100%" }}
              variant="contained"
              fullWidth
              size="large"
              disabled
            >
              <CircularProgress />
              Calculate
            </Button>
          ) : (
            <Button
              sx={{ height: "100%" }}
              variant="contained"
              fullWidth
              size="large"
              onClick={() => submitCalculate()}
            >
              Calculate
            </Button>
          )}
        </Grid>
      </Grid>
    </Box>
  );
};

export default Prime;
