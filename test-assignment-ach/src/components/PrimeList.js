import * as React from "react";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Axios from "axios";
import { useState, useEffect } from "react";

const PrimeList = (props) => {
  useEffect(() => {
    console.log(props);
  }, []);
  return (
    <Grid key={props.RAGE_ID} item md={4}>
      <Box
        sx={{
          backgroundColor: "#FFEAC9",
          p: 2,
          borderRadius: "10px",
        }}
      >
        <Grid container spacing={1}>
          <Grid item xs={12} sx={{ borderBottom: "2px solid black" }}>
            <Grid container spacing={1}>
              <Grid item xs={6}>
                <Typography
                  key={props.ID}
                  variant="h4"
                  gutterBottom
                  component="div"
                >
                  {props.START_NUMBER} : {props.END_NUMBER}
                </Typography>
              </Grid>
              <Grid
                sx={{ display: "flex", justifyContent: "flex-end" }}
                item
                xs={6}
              >
                <Typography
                  key={props.ID}
                  variant="h7"
                  gutterBottom
                  component="div"
                >
                  {props.CREATE_DATE}
                </Typography>
              </Grid>
            </Grid>
          </Grid>

          <Grid item xs={12} sx={{ maxHeight: "750px", overflow: "auto" }}>
            <Grid container spacing={1}>
              {props.PRIME_NUMBER.map((prop2) => {
                return (
                  <Grid item xs={2}>
                    <Typography
                      key={prop2.ID}
                      variant="h6"
                      gutterBottom
                      component="div"
                      sx={{
                        textAlign: "center",
                        borderRadius: "5px",
                        "&:hover": {
                          cursor: "default",
                          backgroundColor: "#FF616D",
                        },
                      }}
                    >
                      {prop2.PRIME_NUMBER}
                    </Typography>
                  </Grid>
                );
              })}
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </Grid>
  );
};

export default PrimeList;
